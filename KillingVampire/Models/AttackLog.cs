﻿using KillingVampire.Characters;

namespace KillingVampire.Models
{
    internal class AttackLog
    {
        public AttackLog(string attackName, Character character, int damage)
        {
            AttackName = attackName;
            Character = character;
            Damage = damage;
        }
        public string AttackName { get; set; }
        public Character Character { get; set; }
        public int Damage { get; set; }
    }
}
