﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KillingVampire.Characters
{
    class Vampire
    {
        private readonly Random _rng;

        public Vampire()
        {
            _rng = new Random();
        }
        public int MaxHp { get; set; }
        public int CurrentHp { get; set; }
        public int DexSave { get; set; }
        public int IntSave { get; set; }
        public int CharSave { get; set; }
        public int BanedForTurns { get; set; }
        public int Ac { get; set; }

        public void Heal(int diceType = -1, int numberOfDice = -1, int scalarHeal = 30)
        {
            if (diceType < 0 || numberOfDice < 0)
            { 
                CurrentHp += scalarHeal;
            }
            else
            {
                for (int i = 0; i < numberOfDice; i++)
                { 
                    CurrentHp += _rng.Next(1, diceType + 1);
                }
            }
            if (CurrentHp > MaxHp)
            { 
                CurrentHp = MaxHp;
            }
        }
    }
}
