﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KillingVampire.Characters
{
    class Character
    {
        private readonly Random _rng;

        public Character(int id, string name, int dc, int toHit)
        {
            Id = id;
            _rng = new Random();
            Name = name;
            ToHit = toHit;
            Dc = dc;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Dc { get; set; }
        public int ToHit { get; set; }

        public int Attack(int enemyAc, int dice, int numberOfDice, int damageModifier)
        {
            var roll = _rng.Next(1, 21);
            var damage = 0;
            if (roll == 20 || roll + ToHit >= enemyAc)
            {
                for (int i = 0; i < numberOfDice; i++)
                {
                    var damageRoll = _rng.Next(1, dice + 1);
                    damage += damageRoll;
                }
                damage += damageModifier;

                if (roll == 20 || roll + ToHit >= enemyAc + 10)
                {
                    damage += dice * numberOfDice;
                }
            }
            return damage;
        }

        public int SavingThrowAttack(int enemyModifier,
            int dice,
            int numberOfDice,
            int damageModifier,
            bool hitByAvaros = false,
            bool baned = false)
        {
            var roll = _rng.Next(1, 21);
            var damage = 0;
            if (hitByAvaros)
            {
                var avarosRoll = _rng.Next(1, 5);
                roll -= avarosRoll;
            }
            //bane
            if (baned)
            {
                var baneRoll = _rng.Next(1, 5);
                roll -= baneRoll;
            }

            if (roll + enemyModifier < Dc)
            {
                for (int i = 0; i < numberOfDice; i++)
                {
                    var diceResult = _rng.Next(1, dice + 1);
                    damage += diceResult;
                }
                damage += damageModifier;
            }

            return damage;
        }



    }
}
