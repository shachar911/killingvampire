﻿using KillingVampire.Characters;
using KillingVampire.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KillingVampire
{
    class Program
    {
        static void Main(string[] _)
        {
            var vampire = new Vampire()
            {
                Ac = 20,
                DexSave = 12,
                IntSave = 4,
                CharSave = 11,
                MaxHp = 311,
                CurrentHp = 311
            };

            var regnar = new Character(1, "Regnar", 18, 10);
            var avaros = new Character(2, "Avaros", 18, 0);
            var destiny = new Character(3, "Destiny", 20, 0);

            var vamDeadCount = 0;
            var diedOnTurns = new List<int>();

            var vampDamagedByLight = false;
            var loop = 1000000;
            var random = new Random();
            //var attacksLog = new Dictionary<int, List<AttackLog>>
            //{
            //    [regnar.Id] = new List<AttackLog>(),
            //    [avaros.Id] = new List<AttackLog>(),
            //    [destiny.Id] = new List<AttackLog>()
            //};
            var bannedTime = 0;
            for (int j = 0; j < loop; j++)
            {
                var vampAlive = true;
                vampire.CurrentHp = vampire.MaxHp;
                var baneUses = 2;
                for (int i = 0; i < 90; i++)
                {

                    vampire.BanedForTurns -= 1;
                    if (vampire.BanedForTurns == -1)
                    {
                        vampire.BanedForTurns = 0;
                    }

                    // avaros  attacks

                    var damage = avaros.SavingThrowAttack(vampire.IntSave, 6, 3, 0, vampire.BanedForTurns > 0);
                    var hitByAvaros = damage > 0;

                    damage /= 2; // resistance

                    vampire.CurrentHp -= damage;
                    // attacksLog[avaros.Id].Add(new AttackLog("avaros.SavingThrowAttack", avaros, damage));
                    if (vampire.CurrentHp <= 0)
                    {
                        vampAlive = false;
                        diedOnTurns.Add(i);
                        break;
                    }

                    if (!vampDamagedByLight)
                    {
                        vampire.Heal();
                    }
                    vampDamagedByLight = false;

                    //regnar attacks

                    if (baneUses > 0 && vampire.BanedForTurns == 0) 
                    {
                        baneUses--;
                        var saveRoll = random.Next(1, 21);
                        if (hitByAvaros)
                        {
                            var avarosRoll = random.Next(1, 5);
                            saveRoll -= avarosRoll;
                        }
                        saveRoll += vampire.CharSave;
                        if (saveRoll < regnar.Dc)
                        {
                            vampire.BanedForTurns = 10;
                            bannedTime++;
                        }

                    }
                    else
                    {


                        var numberOfDice = 0;
                        if (i <= 10)
                        {
                            numberOfDice = 2;
                        }
                        else if (i <= 60)
                        {
                            numberOfDice = 1;
                        }

                        if (numberOfDice > 0)
                        {
                            damage = regnar.Attack(vampire.Ac, 8, numberOfDice, 4);
                            //  attacksLog[regnar.Id].Add(new AttackLog("regnar.Attack", regnar, damage));
                            vampire.CurrentHp -= damage;
                            if (vampire.CurrentHp <= 0)
                            {
                                diedOnTurns.Add(i);
                                vampAlive = false;
                                break;
                            }
                        }
                    }
                    damage = regnar.SavingThrowAttack(vampire.DexSave, 8, 3, 0, hitByAvaros, vampire.BanedForTurns > 0);
                    if (damage > 0)
                    {
                        //  attacksLog[regnar.Id].Add(new AttackLog("regnar.SavingThrowAttack", destiny, damage));
                        vampDamagedByLight = true;
                        vampire.CurrentHp -= damage;
                        if (vampire.CurrentHp <= 0)
                        {
                            diedOnTurns.Add(i);
                            vampAlive = false;
                            break;
                        }
                    }

                    // destiny  attacks
                    damage = destiny.SavingThrowAttack(vampire.DexSave, 8, 3, 0, vampire.BanedForTurns > 0);
                    //  attacksLog[destiny.Id].Add(new AttackLog("destiny.SavingThrowAttack", destiny, damage));

                    if (!vampDamagedByLight && damage > 0)
                    {
                        vampDamagedByLight = true;
                    }
                    vampire.CurrentHp -= damage;
                    if (vampire.CurrentHp <= 0)
                    {
                        diedOnTurns.Add(i);
                        vampAlive = false;
                        break;
                    }


                }
                //Console.WriteLine($"vampAlive: {vampAlive}");
                if (!vampAlive)
                {
                    vamDeadCount++;
                }
            }
            var percent = vamDeadCount * 100.0 / loop;
            var averageDeathTurn = diedOnTurns.Average();

            //var totalDamage = attacksLog.Values.SelectMany(x => x).Sum(x => x.Damage);
            //var regnarAttackPercent = attacksLog[regnar.Id].Where(x => x.AttackName == "regnar.Attack").Sum(x => x.Damage) * 100.0 / totalDamage;
            //var regnarSavePercent = attacksLog[regnar.Id].Where(x => x.AttackName == "regnar.SavingThrowAttack").Sum(x => x.Damage) * 100.0 / totalDamage;
            //var avarosSavePercent = attacksLog[avaros.Id].Where(x => x.AttackName == "avaros.SavingThrowAttack").Sum(x => x.Damage) * 100.0 / totalDamage;
            //var destinySavePercent = attacksLog[destiny.Id].Where(x => x.AttackName == "destiny.SavingThrowAttack").Sum(x => x.Damage) * 100.0 / totalDamage;
            var txt = $"{vamDeadCount} / {loop}, which is {percent}%, AverageDeathTurn: {averageDeathTurn}";
            Console.WriteLine(txt);
            percent = bannedTime * 100.0 / loop;
            Console.WriteLine($"bannedTime: {bannedTime}/{loop} which is {percent}%, ");
            //  Console.WriteLine($"regnarAttackPercent: {regnarAttackPercent}, regnarSavePercent:{regnarSavePercent}, avaros: {avarosSavePercent}, destiny: {destinySavePercent}");
            Console.ReadLine();
            ;
        }
    }
}
